﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190916_Lambda_Books
{
    class Book
    {
        public string Title { get; set; }
        public int Pages { get; set; }
        public int Price { get; set; }
    }
}
