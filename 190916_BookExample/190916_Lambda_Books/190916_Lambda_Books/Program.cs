﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190916_Lambda_Books
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 3, -4, 69, 29, -22, -29, 11, -33, 34, -12, -56, 79 };
            string[] words = { "C# in a nutshell", "Java ist eine Insel", "", "C for Dummies", "JPA 2", "C# Codeblock", "Entwurfsmuster", "" };
            Book[] books;
            InitializeBooks(out books, words);

            int numberOfBooksWithLongNames = Count(books, x => x.Title.Length > 10);
            int numberOfCheapbooks = Count(books, x => x.Price < 10);
            int numberOfNegativeNumbers = Count(numbers, x => x < 0);
            int numberOfEmptyTitles = Count(books, x => x.Title.Length == 0);

            Console.WriteLine("numberOfBooksWithLongNames:" + numberOfBooksWithLongNames);
            Console.WriteLine("numberOfCheapbooks: " + numberOfCheapbooks);
            Console.WriteLine("numberOfNegativeNumbers: " + numberOfNegativeNumbers);
            Console.WriteLine("numberOfEmptyTitles: " + numberOfEmptyTitles);
        }

        public static int Count<T>(T[] arr, Predicate<T> condition)
        {
            int counter = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (condition(arr[i]))
                    counter++;
            }
            return counter;
        }

        public static void InitializeBooks(out Book[] books, string[] words)
        {
            Random r = new Random();
            books = new Book[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                books[i] = new Book() { Title = words[i], Price = r.Next(0, 60), Pages = r.Next(0, 300) };
            }
        }
    }
}
