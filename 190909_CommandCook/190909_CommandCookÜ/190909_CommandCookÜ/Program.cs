﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_CommandCookÜ
{
    class Program
    {
        static void Main(string[] args)
        {
                Waiter waiter = new Waiter();
                Cook cook = new Cook();
                Customer customer = new Customer("Michael");
                Order order = new Order("Ice Tea");
                Food f = waiter.takeOrder(customer, order, cook);

                Console.WriteLine(f);

        }
    }
}
