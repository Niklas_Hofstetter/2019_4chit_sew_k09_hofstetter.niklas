﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_CommandCookÜ
{
    public class Customer
    {
        private String name;

        public Customer (String name)
        {
            this.name = name;
        }
        public string Name { get => name; }
    }
}
