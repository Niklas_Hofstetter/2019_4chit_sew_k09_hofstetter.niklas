﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_CommandCookÜ
{
    public class Waiter
    {
        public Food takeOrder(Customer cust, Order order, Cook cook)
        {
            Food food = cook.PrepOrder(order, this);
            return food;
        }
    }
}
