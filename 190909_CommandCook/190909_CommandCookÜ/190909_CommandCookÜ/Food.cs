﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_CommandCookÜ
{
    public class Food
    {
        Order order;
        public Food()
        {

        }
        public Food(Order order)
        {
            this.order = order;
        }
        public override String ToString()
        {
            return order.getCommand() + " has completed";
        }
        public void setOrder(Order order)
        {
            this.order = order;
        }
        public Order getOrder()
        {
            return order;
        }

    }
}
