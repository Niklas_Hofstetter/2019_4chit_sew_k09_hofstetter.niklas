﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _200416_EFTEst
{
    public class DrinkContext : DbContext
    {
        public DbSet<Drinks> Drinks { get; set; }

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=DrinkDB;Trusted_Connection=True;");
        }
    }
}
