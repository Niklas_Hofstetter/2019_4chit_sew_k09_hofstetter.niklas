﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _200416_EFTEst
{
    public static class DrinkInitializer 
    {
        public static void Seed(DrinkContext context)
        {
            context.Add(new Drinks { Name = "Cola", Price = 2});
            context.SaveChanges();
        }
    }
}
