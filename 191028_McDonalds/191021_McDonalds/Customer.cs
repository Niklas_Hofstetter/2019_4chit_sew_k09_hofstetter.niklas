﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191021_McDonalds
{
    public class Customer
    {
        public string name;
        public Customer(string n)
        {
            name = n;
        }

        public void Order()
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("\t" + Thread.CurrentThread.Name + " stellt sich an ");
            Program.semEmployee.Wait();
            Program.semCust.Release();
            Program.semCounter.Wait();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(Thread.CurrentThread.Name + " Bestellung aufgeben ");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(Thread.CurrentThread.Name + " Bestellung erhalten ");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(Thread.CurrentThread.Name + " geht das Menü essen ");
            Program.semEmployee.Release();
        }
    }
}
