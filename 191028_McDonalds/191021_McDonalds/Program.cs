﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191021_McDonalds
{
    class Program
    {
        static int amount = 10;
        static int counterAmount = 3;

        public static SemaphoreSlim semCust = new SemaphoreSlim(0);
        public static SemaphoreSlim semEmployee = new SemaphoreSlim(counterAmount);
        public static SemaphoreSlim semCounter = new SemaphoreSlim(0);

        //public static void InitCustomerThreads(out Customer[] bus, out Thread[] busthread, int amount)

        static void Main(string[] args)
        {
            Customer[] bus;
            Thread[] busthreads;

            InitCustomerThreads(out bus, out busthreads, amount);
            StartCustomerThreads(busthreads);

            char a = 'A';
            for (int i = 0; i < counterAmount; i++)
            {
                string gatename = String.Format("GATE {0}{1}{2}", a, a, a);
                Thread counter1 = StartMcDCounter(gatename);
                a++;
            }

        }
        public static void InitCustomerThreads(out Customer[]bus, out Thread[] busthread, int amount)
        {
            bus = new Customer[amount];
            busthread = new Thread[amount];
            for (int i = 0; i < amount; i++)
            {
                bus[i] = new Customer("Customer" + i);
                busthread[i] = new Thread(bus[i].Order);
                busthread[i].Name = "Customer" + i;
            }
        }
        public static void StartCustomerThreads(Thread[] busthread)
        {
            int i = 0;
            while(i < busthread.Length && busthread[i]!=null)
            {
                busthread[i].Start();
                i++;
            }
        }
        public static Thread StartMcDCounter(string name)
        {
            Thread gate1 = new Thread(Employee.TakeOrder);
            gate1.Name = name;
            gate1.Start();
            return gate1;
        }
    }
}
