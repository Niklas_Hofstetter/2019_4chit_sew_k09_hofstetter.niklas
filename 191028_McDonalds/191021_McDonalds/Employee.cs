﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191021_McDonalds
{
    public class Employee
    {
        public static void TakeOrder()
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(Thread.CurrentThread.Name + " FREI");

                Program.semCust.Wait();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" ---" + Thread.CurrentThread.Name + " nimmt Bestellung entgegen");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" ---" + Thread.CurrentThread.Name + " gibt Bestellung aus: ");
                Program.semCounter.Release();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(Thread.CurrentThread.Name + " wieder FREI");
            }
        }

    }
}
