﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_StateGumBall
{
    public interface IState
    {
        void insertQuarter();
        void ejectQuarter();
        void turnCrank();
        void dispense();
    }
    public class SoldState:IState
    {
        GumballMachine gm;
        public SoldState(GumballMachine g)
        {
            gm = g;
        }
        public void insertQuarter()
        {
            Console.WriteLine("Please wait, we´re already giving you a gumball");
        }
        public void ejectQuarter()
        {
            Console.WriteLine("Sorry, you already turned the crank");
        }
        public void turnCrank()
        {
            Console.WriteLine("Turning twice doesn´t get you another gumball");
        }
        public void dispense()
        {
            
            gm.releaseBall(); 
            if (gm.getCount() > 0)
            {
                gm.setState(gm.getNoQuarterState());
            }
            else
            {
                Console.WriteLine("Oops, out of gumballs!");
                gm.setState(gm.getSoldOutState());
            }

        }
    }
    public class SoldOutState : IState
    {
        GumballMachine gm;
        public SoldOutState(GumballMachine g)
        {
            gm = g;
        }
        public void insertQuarter()
        {
            Console.WriteLine("You can´t insert another quarter");
        }
        public void ejectQuarter()
        {
            Console.WriteLine("Quarter returned");
            gm.setState(gm.getNoQuarterState());
        }
        public void turnCrank()
        {
            Console.WriteLine("Sorry, we´re sold out");
        }
        public void dispense()
        {
            Console.WriteLine("Sorry, we´re sold out");
        }
    }
    public class NoQuarterState : IState
    {
        GumballMachine gm;
        public NoQuarterState(GumballMachine g)
        {
            gm = g;
        }
        public void insertQuarter()
        {
            Console.WriteLine("You inserted a quarter");
            gm.setState(gm.getHasQuarterState());
        }
        public void ejectQuarter()
        {
            Console.WriteLine("You haven´t inserted a quarter");
        }
        public void turnCrank()
        {
            Console.WriteLine("You turned, but there is no quarter");
        }
        public void dispense()
        {
            Console.WriteLine("You need to pay first");
        }
    }
    public class HasQuarterState : IState
    {
        GumballMachine gm;
        public HasQuarterState(GumballMachine g)
        {
            gm = g;
        }
        public void insertQuarter()
        {
            Console.WriteLine("You can´t insert another quarter");
        }
        public void ejectQuarter()
        {
            Console.WriteLine("Quarter returned");
            gm.setState(gm.getNoQuarterState());
        }
        public void turnCrank()
        {
            Console.WriteLine("You turned...");
            gm.setState(gm.getSoldState());
        }
        public void dispense()
        {
            Console.WriteLine("No gumball dispensed");
        }
    }
}
