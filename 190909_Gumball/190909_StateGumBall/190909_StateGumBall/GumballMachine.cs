﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190909_StateGumBall
{
    public class GumballMachine
    {
        IState soldOutState;
        IState noQuarterState;
        IState hasQuarterState;
        IState soldState;
        IState state;

        int count = 0;
        public GumballMachine(int numberGumballs)
        {
            
            soldOutState = new SoldOutState(this);
            noQuarterState = new NoQuarterState(this);
            hasQuarterState = new HasQuarterState(this);
            soldState = new SoldState(this);
            this.count = numberGumballs;
            if (numberGumballs > 0)
            {
                state = noQuarterState;
            }
        }
        public void insertQuarter()
        {
            state.insertQuarter();
        }
        public void ejectQuarter()
        {
            state.ejectQuarter();
        }
        public void turnCrank()
        {
            state.turnCrank();
            state.dispense();
        }
        public void setState(IState state)
        {
            this.state = state;
        }
        public void releaseBall()
        {
            Console.WriteLine("A gumball comes rolling out the slot...");
            if (count != 0)
                count -= 1;
        }
        public IState getSoldOutState()
        {
            return soldOutState;
        }
        public IState getNoQuarterState()
        {
            return noQuarterState;
        }
        public int getCount()
        {
            return count;
        }
        public IState getHasQuarterState()
        {
            return hasQuarterState;
        }
        public IState getSoldState()
        {
            return soldState;
        }
        public override string ToString()
        {
            return "Ich bin eine Gumball Maschine und habe gerade den Zustand " + state;
        }
    }
}
