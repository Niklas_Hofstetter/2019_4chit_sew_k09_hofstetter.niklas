﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace _20200302_MDF
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection;
            connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Niklas\Documents\Database.mdf;Integrated Security=True;Connect Timeout=30");
            connection.Open();

            Insert(connection);
            Select(connection);
            Truncate(connection);

            connection.Close();
            
        }

        private static void Insert(SqlConnection connection)
        {
            SqlCommand insert = new SqlCommand(
                "INSERT INTO Users(Name, Type) " +
                "VALUES ('Tim Denton', 'Tester'), " +
                "('Tara Totue', 'Tester')," +
                "('Sarah Trotter', 'Developer')," +
                " ('John Dahla', 'Developer')," +
                "('Mary Malcopp', 'Manager')," +
                "('Colin Carton', 'Customer')", connection
                );
            insert.ExecuteNonQuery();
        }

        private static void Truncate(SqlConnection con)
        {
            SqlCommand truncate =
                new SqlCommand("TRUNCATE TABLE Users", con);
            truncate.ExecuteNonQuery();
        }

        private static void Select(SqlConnection connection)
        {
            SqlCommand select = new SqlCommand("SELECT * FROM Users", connection);
            List<User> users = new List<User>();
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    users.Add(new User()
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Name = reader["Name"].ToString(),
                        Type = reader["Type"].ToString()

                    });

                }
            }
            var xmlUsers = new XElement("users",
            users.Select(u => new XElement("user",
            new XAttribute("name", u.Name),
            new XAttribute("type", u.Type)))
            );

            using (StreamWriter sw = new StreamWriter("users.xml", false))
            {
                sw.Write(xmlUsers);
            }
        }
    }
}
