﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _190923_ProductExample
{
    public class Product
    {
        private string name;
        private double price;
        private Tags tag;

        public Product(string name, double price, Tags tag)
        {
            this.Name = name;
            this.Price = price;
            this.Tag = tag;
        }

        public Product()
        {
            this.Name = "";
            this.price = 0;
            this.Tag = Tags.none;
        }

        public string Name { get => name; set => name = value;}

        public double Price { get => price;
            set {
                if (value >= 0)
                    price = value;
                else throw new Exception("Invalid Value!");
            }
        }

        public Tags Tag { get => tag; set => tag = value;}

    }
}
