﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _190923_ProductExample
{
    public enum Tags {
        [Description("This product doesn't have any tags")]
        none,

        [Description("This product is old")]
        old,

        [Description("This product is limited")]
        limited,

        [Description("This product is overpriced")]
        overpriced,

        [Description("This product is overrated")]
        overrated
    }
}
