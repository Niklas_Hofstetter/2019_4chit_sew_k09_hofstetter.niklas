﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _190923_ProductExample
{
    public class ProductVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Product> products;
        private Product currentProduct;
        private List<Tags> tagTemplates;
        public ICommand AddToListCommand { get; set; }

        public ProductVM()
        {
            Products = new ObservableCollection<Product>();
            currentProduct = new Product();
            tagTemplates = new List<Tags> { Tags.none, Tags.old, Tags.limited, Tags.overpriced, Tags.overrated };
            CurrentTag = tagTemplates[0];
            AddToListCommand = new RelayCommand ( o =>
                {
                    products.Add(currentProduct);
                    CurrentProduct = new Product();
                    CurrentTag = tagTemplates[0];
                },
                o=>!String.IsNullOrEmpty(CurrentName) && CurrentPrice > 0
            );
        }

        public ObservableCollection<Product> Products { get => products; set { products = value; OnPropertychanged(); } }

        public void OnPropertychanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string CurrentName
        {
            get => currentProduct.Name;
            set
            {
                currentProduct.Name = value;
                OnPropertychanged();
            }
        }

        public double CurrentPrice
        {
            get => currentProduct.Price;
            set
            {
                currentProduct.Price = value;
                OnPropertychanged();
            }
        }

        public Tags CurrentTag
        {
            get => currentProduct.Tag;
            set
            {
                currentProduct.Tag = value;
                OnPropertychanged();
            }
        }

        public Product CurrentProduct
        {
            get => currentProduct;
            set
            {
                currentProduct = value;
                OnPropertychanged();
                OnPropertychanged("CurrentName");
                OnPropertychanged("CurrentPrice");
                OnPropertychanged("CurrentTag");
            }
        }

        public List<Tags> TagTemplates { get => tagTemplates; }
    }
}
