﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _120520
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (var context = new SchoolContext())
            //{
            //    var std = new Student()
            //    {
            //        Name = "Niklas",
            //        Age = 17,
            //        DateOfBirth = new DateTime(2002, 05, 21),
            //        Height = 179,
            //        Weight = 55
            //    };

            //    context.Students.Add(std);
            //    context.SaveChanges();
            //}

            using (var context = new SchoolContext())
            {
                var std = new Student() { Name = "Steve" };
                context.Add(std);

                context.SaveChanges();
                Console.ReadLine();
            }

        }
        static void Query()
        {
            var context = new SchoolContext();
            var studentsWithSameName = context.Students
                                              .Where(s => s.Name == "Niklas")
                                              .ToList()
                                              .FirstOrDefault();
            Console.WriteLine(studentsWithSameName);
        }
        static void UpdateData()
        {
            using (var context = new SchoolContext())
            {
                var std = context.Students.First<Student>();
                std.Name = "Jochen";
                context.SaveChanges();
            }
        }

        static void DeleteData()
        {
            using (var context = new SchoolContext())
            {
                var std = context.Students.First<Student>();
                context.Students.Remove(std);

                // or
                // context.Remove<Student>(std);

                context.SaveChanges();
            }
        }
    }
}
