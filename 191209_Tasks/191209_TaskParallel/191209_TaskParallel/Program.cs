﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191209_TaskParallel
{
    class Program
    {
        static Random r = new Random();
        static int[] array = new int[10];
        static void Main(string[] args)
        {
            
                var cancelSource = new CancellationTokenSource();
                CancellationToken token = cancelSource.Token;

                Task<int> task1 = Task.Factory.StartNew<int>(() => { return r.Next(0, 11) * 2; });
                Task<int> task2 = task1.ContinueWith<int>(ant => { return ant.Result - 10; }, token);
                if (task1.Result > 10)
                    cancelSource.Cancel();
              try
            {
                task2.Wait();
                Console.WriteLine(task2.Result);
            }
            catch (AggregateException e)
            {
                if(e.InnerException is OperationCanceledException)
                    Console.WriteLine("Second task is canceled!");
                Console.WriteLine(task1.Result);
            }
            Parallel.For(0, array.Length, Square);
            Console.WriteLine();
            for (int i = 0; i < array.Length;i ++)
            {
                Console.WriteLine(array[i] + " ");
            }
            Console.ReadKey();
        }
        static void Square(int i)
        {
            array[i] = (int)Math.Pow(array[i], 2);
        }
    }
}
