﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191209_Threadpool
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 20; i++)
            {
                Task.Factory.StartNew(Wait);
            }
            Console.ReadKey();
            
        }
        static void Wait()
        {
            Console.WriteLine("Task started!");
            Thread.Sleep(2000);
            Console.WriteLine("Task finished!");
        }
    }
}
