﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _191021_Abmalbeispiel
{
    class Program
    {
        static void Main(string[] args)
        {
            int ships = 10;
            for (int i = 0; i < ships; i++)
            {
                Thread s = new Thread(new Ship().Run);
                s.Name = "Ship" + (i + 1);
                s.Start();
            }
            Thread h = new Thread(new Harbor().Run);
            h.Name = "Harbor";
            h.Start();
        }
        public class Harbor
        {
            public void Run()
            {
                while (true)
                {
                    Signal();
                    Unload();
                }
            }
            public void Signal()
            {
                Console.WriteLine("{0} signal", Thread.CurrentThread.Name);
            }
            public void Unload()
            {
                Console.WriteLine("{0} unloads", Thread.CurrentThread.Name);
                Thread.Sleep(2000);
            }
        }
        public class Ship
        {
            public void Run()
            {
                Unload();
                Leave();
            }
            public void Leave()
            {
                Console.WriteLine("{0} leaves", Thread.CurrentThread.Name);
            }
            public void Unload()
            {
                Console.WriteLine("{0} unloads", Thread.CurrentThread.Name);
                Thread.Sleep(2000);
            }
        }
    }
}
