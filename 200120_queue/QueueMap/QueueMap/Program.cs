﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueueMap
{

    class Program
    {
        static void Main(string[] args)
        {

            Ex10 x = new Ex10();
            x.Methode();

            EX11 x2 = new EX11();
            x2.Methode();

            EX8 x3 = new EX8();

            FISCHA.Test();
            
        }
    }
    public class UserInfo{
        public string Info { get; set; }
        public string Adress { get; set; }
    }
    public class UserService
    {
        internal static async Task<string> GetUserAsync(string user)
        {
            return await Task.FromResult(user);
        }
    }
    public class AdressService
    {
        internal static async Task<string> GetAdressAsync(string user)
        {
            return await Task.FromResult(user);
        }
    }
    public class UserSearch
    {
        public async Task<UserInfo> GetUserInfoAsync(string term, List<string> names)
        {
            var userName =
                (from name in names
                 where name.StartsWith(term)
                 select name).FirstOrDefault();
            var user = new UserInfo();
            user.Info = await UserService.GetUserAsync(userName).ConfigureAwait(false);
            user.Adress = await AdressService.GetAdressAsync(userName);
            return user;
        }
    }
    public class EX8
    {
        public void Methode()
        {
            Queue<string> myQueue = new Queue<string>();
            myQueue.Enqueue("Item1");
            myQueue.Enqueue("Item2");
            myQueue.Enqueue("Item3");
            myQueue.Enqueue("Item4");
            Console.WriteLine("On the queue are " + myQueue.Count() + " items");
            myQueue.Dequeue();
            Console.WriteLine("On the queue are " + myQueue.Count() + " items");
            Console.WriteLine(myQueue.Peek());
            while (myQueue.Count() > 0)
            {
                string s = myQueue.Dequeue();
                Console.WriteLine("Dequeueing " + s);
            }
        }
}
    public class Ex9
    {
        public static BlockingCollection<int> data = new BlockingCollection<int>();
        private static void Producer()
        {
            int amount = 10;
            for (int i = 0; i < amount; i++)
            {
                data.Add(i);
                Thread.Sleep(100);
            }
        }
        private static void Consumer()
        {
            foreach(var item in data.GetConsumingEnumerable())
            {
                Console.WriteLine(item);
            }
        }
        public static void Test()
        {
            var producer = Task.Factory.StartNew(() => Producer());
            var consumer = Task.Factory.StartNew(() => Consumer());
            Console.Read();
        }

    }
    public class Ex10
    {
        public void Methode()
        {
            BlockingCollection<int>[] producers = new BlockingCollection<int>[3];
            producers[0] = new BlockingCollection<int>(boundedCapacity: 10);
            producers[1] = new BlockingCollection<int>(boundedCapacity: 10);
            producers[2] = new BlockingCollection<int>(boundedCapacity: 10);
            Task t1 = Task.Factory.StartNew(() =>
            {
                for (int i = 1; i <= 10; i++)
                {
                    producers[0].Add(i);
                    Thread.Sleep(100);
                }
                producers[0].CompleteAdding();
            });
            Task t2 = Task.Factory.StartNew(() =>
            {
                for (int i = 11; i <= 20; i++)
                {
                    producers[1].Add(i);
                    Thread.Sleep(150);
                }
                producers[1].CompleteAdding();
            });
            Task t3 = Task.Factory.StartNew(() =>
            {
                for (int i = 21; i <= 30; i++)
                {
                    producers[2].Add(i);
                    Thread.Sleep(100);
                }
                producers[2].CompleteAdding();
            });
            while(!producers[0].IsCompleted ||
                !producers[1].IsCompleted ||
                !producers[2].IsCompleted)
                {
                int item;
                BlockingCollection<int>.TryTakeFromAny(producers, out item, TimeSpan.FromSeconds(1));
                if (item != default(int))
                {
                    Console.WriteLine(item);
                }
                {

                }
            }
        }
    }
    public class EX11
    {
        public void Methode()
        {
            Queue<int> qt = new Queue<int>();
            qt.Enqueue(1);
            qt.Enqueue(2);
            qt.Enqueue(3);

            foreach(Object obj in qt)
            {
                Console.WriteLine(obj);
            }
            Console.WriteLine();
            Console.WriteLine("The Number of elements in the queue " + qt.Count);
            Console.WriteLine("Does the queue contain " + qt.Contains(4));
        }
    }
    public static class FISCHA
    {
        public static void Test()
        {
            Stopwatch s = new Stopwatch();

            Console.WriteLine("TestWithoutAsync");
            s.Start();
            TestWithoutAsync();
            s.Stop();
            Console.WriteLine("TestWithoutAsync: " + s.ElapsedMilliseconds);
            s.Reset();

            Console.WriteLine("TestWithAsync");
            s.Start();
            TestWithAsync();
            s.Stop();
            Console.WriteLine("TestWithAsync: " + s.ElapsedMilliseconds);
        }

        static async Task<int> Add(int x, int y)
        {
            Task<int> t = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                return x + y;
            });
            return await t;
        }

        static async Task<int> Sub(int x, int y)
        {
            Task<int> t = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                return x - y;
            });
            return await t;
        }
        static async Task<int> Mult(int x, int y)
        {
            Task<int> t = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                return x * y;
            });
            return await t;
        }

        static async Task TestWithAsync()
        {
            Console.WriteLine(await Add(2, 3));
            Console.WriteLine(await Sub(2, 3));
            Console.WriteLine(await Mult(2, 3));
        }

        static void TestWithoutAsync()
        {
            Console.WriteLine(Add(2, 3).Result);
            Console.WriteLine(Sub(2, 3).Result);
            Console.WriteLine(Mult(2, 3).Result);
        }
    }
}
