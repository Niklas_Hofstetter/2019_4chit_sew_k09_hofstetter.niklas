﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingCart
{
    delegate Func<int> DiscountDelegate();
    class Program
    {
        static void Main(string[] args)
        {
            new ShoppingCart().Process(
                new Func<bool, int>(delegate(bool x) { return x ? 10 : 5; }
                ));
            Console.WriteLine("Der Discount beträgt " + Calculator.Calculate(true) + "%");
        }
    }
    public class Calculator
    {
        public static int Calculate(bool special)
        {
            int discount = 0;
            if (DateTime.Now.Hour < 12)
                discount = 5;
            else if (DateTime.Now.Hour < 20)
                discount = 10;
            else if (special)
                discount = 20;
            else
                discount = 15;

            return discount;
        }
    }
}
