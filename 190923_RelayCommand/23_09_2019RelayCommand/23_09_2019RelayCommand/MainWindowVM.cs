﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace _23_09_2019RelayCommand
{
    class MainWindowVM 
    {
        public string Name { get; set; }

        public ICommand HelloCommand { get; set; }
        public ICommand NameCommand { get; set; }

        public MainWindowVM()
        {
            HelloCommand = new RelayCommand(o => MessageBox.Show("Hallo Welt!"), o => true);
            NameCommand = new RelayCommand(o => MessageBox.Show("Hallo " + Name), o => !String.IsNullOrEmpty(Name));
        }
        
    }
}
