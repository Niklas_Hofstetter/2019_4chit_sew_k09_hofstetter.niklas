﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _071019_pizzeria
{
    public abstract class Pizza
    {
        public string name;
        public double price;
        public List<string> layer = new List<string>();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Name: ").Append(name).Append("; Preis: ").Append(price).Append("; Belag: ");
            foreach(string x in layer)
            {
                sb.Append(x).Append(", ");
            }
            return sb.ToString();
        }

        public void Prepare()
        {
            Console.WriteLine(ToString());
        }

    }
    class Salami : Pizza
    {
        public Salami()
        {
            name = "Salami";
            price = 3.99;
            layer.Add("Tomatensauce");
            layer.Add("Käse");
            layer.Add("Salami");
        }
    }

    class Hawaii : Pizza
    {
        public Hawaii()
        {
            name = "Hawaii";
            price = 5.00;
            layer.Add("Käse");
            layer.Add("Schinken");
            layer.Add("Ananas");
        }
    }
    class Schinken : Pizza
    {
        public Schinken()
        {
            name = "Schinken";
            price = 2.99;
            layer.Add("Tomatensauce");
            layer.Add("Schinken");
            layer.Add("Käse");
        }
    }
    class Margherita : Pizza
    {
        public Margherita()
        {
            name = "Margherita";
            price = 4.99;
            layer.Add("Tomatensauce");
            layer.Add("Käse");
        }
    }
    class Tonno : Pizza
    {
        public Tonno()
        {
            name = "Tonno";
            price = 6.99;
            layer.Add("Tomatensauce");
            layer.Add("Käse");
            layer.Add("Thunfisch");
        }
    }
}
