﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _071019_pizzeria
{
    public abstract class Pizzeria
    {
        protected abstract Pizza CreatePizza(string type);
        public Pizza GetPizza(string type)
        {
            Pizza pizza = CreatePizza(type);

            pizza.Prepare();
            return pizza;
        }
    }
    class HamHam : Pizzeria
    {
        protected override Pizza CreatePizza(String type)
        {
            Pizza pizza = null;
            switch (type)
            {
                case "Tonno":
                    pizza = new Tonno();
                    break;
                case "Salami":
                    pizza = new Salami();
                    break;
                case "Schinken":
                    pizza = new Schinken();
                    break;
                case "Hawaii":
                    pizza = new Hawaii();
                    break;
                default:
                    break;
            }

            return pizza;
        }
    }
    class Trollstiege : Pizzeria
    {
        protected override Pizza CreatePizza(String type)
        {
            Pizza pizza = null;
            switch (type)
            {
                case "Salami":
                    pizza = new Salami();
                    break;
                case "Schinken":
                    pizza = new Schinken();
                    break;
                case "Margherita":
                    pizza = new Margherita();
                    break;
                default:
                    break;
            }

            return pizza;
        }
    }
}
