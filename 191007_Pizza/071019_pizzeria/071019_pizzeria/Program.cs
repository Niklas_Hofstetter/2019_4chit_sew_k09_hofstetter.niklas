﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _071019_pizzeria
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizzeria Trollstiege = new Trollstiege();
            Pizzeria HamHam = new HamHam();

            Trollstiege.GetPizza("Salami");
            Console.WriteLine();
            //Trollstiege.GetPizza("Tonno");
            Trollstiege.GetPizza("Margherita");
            Console.WriteLine();
            HamHam.GetPizza("Tonno");
            Console.WriteLine();
            HamHam.GetPizza("Schinken");
            Console.WriteLine();
            HamHam.GetPizza("Hawaii");
        }
    }
}
