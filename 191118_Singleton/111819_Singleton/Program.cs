﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _111819_Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Singleton myInstance = Singleton.Instance;
            myInstance.Name = "LMAO";
            //Console.WriteLine(myInstance.Name);

            ThreadTest t = new ThreadTest();
            ThreadData d = new ThreadData();
            d.Test();
            d.Test2();

            #region LambdaExpr
            for (int i = 0; i < 10; i++)
                new Thread(() => Console.Write(i)).Start();
            Console.WriteLine();

            for (int i = 0; i < 10; i++)
            {
                int temp = i;
                new Thread(() => Console.Write(temp)).Start();
            }
            #endregion
            #region Priority
            //enum ThreadPriority { Lowest, BelowNormal, Normal, AboveNormal, Highest }
            #endregion
        Console.ReadKey();

        }
    }
    class Singleton
    {
        public string Name { get; set; }
        private Singleton() { }
        private static Singleton instance = null;

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }
    }
    class ThreadTest
    {
        public void Test()
        {
            Thread t = new Thread(WriteY); // Kick off a new thread
            t.Start(); // running WriteY()
                       // Simultaneously, do something on the main thread.
            for (int i = 0; i < 1000; i++) Console.Write("x");
        }
        public void WriteY()
        {
            for (int i = 0; i < 1000; i++) Console.Write("y");
        }


        bool done;
        public void Test2()
        {
            ThreadTest tt = new ThreadTest(); // Create a common instance
            new Thread(tt.Go).Start();
            tt.Go();
        }
        // Note that Go is now an instance method
        void Go()
        {
            if (!done) { done = true; Console.WriteLine("Done"); }
        }


        static bool done3; // Static fields are shared between all threads
        public void Test3()
        {
            new Thread(Go3).Start();
            Go3();
        }
        static void Go3()
        {
            if (!done3) { done3 = true; Console.WriteLine("Done"); }
        }
    }
    class ThreadSafe
    {
        static bool done;
        static readonly object locker = new object();
        public void Test()
        {
            new Thread(Go).Start();
            Go();
        }
        static void Go()
        {
            lock (locker)
            {
                if (!done) { Console.WriteLine("Done"); done = true; }
            }
        }
    }
    class ThreadJoin
    {
        public void Test()
        {
            Thread t = new Thread(Go);
            t.Start();
            t.Join();
            Console.WriteLine("Thread t has ended!");
        }
        static void Go()
        {
            for (int i = 0; i < 1000; i++) Console.Write("y");
        }
    }
    class ThreadData
    {
        public void Test()
        {
            Thread t = new Thread(() => Print("Hello from t!"));
            t.Start();
        }
        static void Print(string message)
        {
            Console.WriteLine(message);
        }

        public void Test2()
        {
            Thread t = new Thread(Print2);
            t.Start("Hello from t!");
        }
        static void Print2(object messageObj)
        {
            string message = (string)messageObj; // We need to cast here
            Console.WriteLine(message);
        }
    }
    class ThreadNaming
    {
        public void Test()
        {
            Thread.CurrentThread.Name = "main";
            Thread worker = new Thread(Go);
            worker.Name = "worker";
            worker.Start();
            Go();
        }
        static void Go()
        {
            Console.WriteLine("Hello from " + Thread.CurrentThread.Name);
        }
    }


}
