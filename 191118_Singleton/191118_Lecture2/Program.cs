﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _191118_Lecture2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Deadlocks
            object locker1 = new object();
            object locker2 = new object();
            new Thread(() =>
            {
                lock (locker1)
                {
                    Thread.Sleep(1000);
                    lock (locker2) ; // Deadlock
                }
            }).Start();
            lock (locker2)
            {
                Thread.Sleep(1000);
                lock (locker1) ;
            }
            #endregion
        }
        class ThreadUnsafe
        {
            static int _val1 = 1, _val2 = 1;
            static void Go()
            {
                if (_val2 != 0) Console.WriteLine(_val1 / _val2);
                _val2 = 0;
            }

        }
        class ThreadSafe
        {
            static readonly object _locker = new object();
            static int _val1, _val2;
            static void Go()
            {
                lock (_locker)
                {
                    if (_val2 != 0) Console.WriteLine(_val1 / _val2);
                    _val2 = 0;
                }
            }
        }
    }
}
