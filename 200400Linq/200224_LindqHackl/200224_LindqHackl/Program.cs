﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace _200224_LindqHackl
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Virus> viruses = new List<Virus>();
            viruses.Add(new Virus() { Id = 1, Description = "Kohannes Jurz", IsDangerous = true, Patients = 88 });
            viruses.Add(new Virus() { Id = 2, Description = "Fristoph Chischer ", IsDangerous = false, Patients = 420});
            viruses.Add(new Virus() { Id = 3, Description = "Kobias Tastl", IsDangerous = false, Patients = 4 });
            viruses.Add(new Virus() { Id = 4, Description = "Blorian Fruckner ", IsDangerous = true, Patients = 187 });
            viruses.Add(new Virus() { Id = 5, Description = "Satrick Peper", IsDangerous = true, Patients = 19 });
            viruses.Add(new Virus() { Id = 6, Description = "Hiklas Nofstetter", IsDangerous = true, Patients = 9 });
            viruses.Add(new Virus() { Id = 7, Description = "Grichael Mötzl", IsDangerous = false, Patients = 139 });
            viruses.Add(new Virus() { Id = 8, Description = "Schobias Trammel", IsDangerous = true, Patients = 23 });
            viruses.Add(new Virus() { Id = 9, Description = "Hlaus Kuber", IsDangerous = true, Patients = 53 });
            viruses.Add(new Virus() { Id = 10, Description = "Plorian Foppinger", IsDangerous = false, Patients = 110 });

            var xEle = new XElement("Viruses", from vir in viruses
                                                select new XElement("Virus",
                                                new XAttribute("Id", vir.Id),
                                                new XElement("Description", vir.Description),
                                                new XElement("IsDangerous", vir.IsDangerous),
                                                new XElement("Patients", vir.Patients)
                                                )
                                                );

            xEle.Save("File.xml");
            XDocument rDoc = XDocument.Load("File.xml");
            IEnumerable<XElement> emps = rDoc.Elements();

            foreach (var e in emps)
            {
                Console.WriteLine(e);
            }


        }
    }
}
