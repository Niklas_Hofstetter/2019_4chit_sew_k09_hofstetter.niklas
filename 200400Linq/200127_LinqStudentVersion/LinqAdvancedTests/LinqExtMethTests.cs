﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class LinqExtensionMethodsTests
    {
        [TestMethod()]
        public void SortNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            for (int i = 0; i < names.Length - 1; i++)
            {
                int dif = String.Compare(names[i], names[i + 1]);
                bool sorted = dif < 0;
                Assert.IsTrue(sorted, "Names are not equal");
            }
        }

        [TestMethod()]
        public void ConcatNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());
            string[] result = LinqExtensionMethods.ConcatNames(names, pupils);

            //Test ConcatNames Method
            Assert.AreEqual(result.Length, (pupils.Length+names.Length), "Concat didn't work");

            //Use the Length of the collections: names+pupils == result 
            foreach (var item in result)
            {
                Console.WriteLine(item+ ", ");
            }
        }

        [TestMethod()]
        public void RemoveRedundantTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            
            //Test remove redundant
            List<string> result = LinqExtensionMethods.RemoveRedundant(NameGenerator.GetNames()).ToList(); 

            //if you take out element by element - every one should be unique
            //so there should not be a second one with the same value in the list
            while (result.Count > 1)
            {
                string s = result.First();
                result.Remove(result.First());
                Assert.IsFalse(result.Contains(s));
                
            }
        }

        [TestMethod()]
        public void UnionNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());

            //Use Concat & Remove 
            string[] concat = LinqExtensionMethods.ConcatNames(names, pupils);
            string[] result = LinqExtensionMethods.RemoveRedundant(concat);

            //Test UnionNames 
            string[] union = LinqExtensionMethods.UnionNames(names, pupils);

            //Both collections should have the same amount of elements (result == union)
            Assert.AreEqual(union.Length, result.Length);
        }

        [TestMethod()]
        public void ReverseNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            //Test ReverseName Method
            string[] result = LinqExtensionMethods.ReverseNames(names);

            //First element should now be the last element
            Assert.AreEqual(names[0], result[result.Length - 1]);
        }
    }

    [TestClass()]
    public class TestPetMethods
    {
        [TestMethod()]
        public void SortPetsTest()
        {
            Pet[] names = Pet.GetCats();
            for (int i = 0; i < names.Length - 1; i++)
            {
                int dif = String.Compare(names[i].Name, names[i + 1].Name);
                bool sorted = dif < 0;
                Assert.IsTrue(sorted, "Names are not equal");
            }
        }
        [TestMethod()]
        public void ConcatPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] result = LinqExtensionMethods.PetConcaterClass.ConcatArrays(cats, dogs).ToArray();

            Assert.AreEqual(result.Length, (cats.Length + dogs.Length), "Concat didn't work");
            
        }
        [TestMethod()]
        public void UnionPets()
        {
            Pet[] dogs = Pet.GetDogs();
            Pet[] cats = Pet.GetCats();

            //Use Concat & Remove 
            string[] concat = LinqExtensionMethods.PetConcaterClass.ConcatArrays(cats, dogs).ToArray();
            string[] result = concat.Distinct().ToArray();

            //Test UnionNames 
            string[] union = LinqExtensionMethods.PetConcaterClass.Union(dogs, cats).ToArray();

            //Both collections should have the same amount of elements (result == union)
            Assert.AreEqual(union.Length, result.Length);
        }
        [TestMethod()]
        public void IntersectPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] result = LinqExtensionMethods.PetConcaterClass.Intersect(cats, dogs).ToArray();

            foreach (var item in result)
            {
                Assert.IsTrue(cats.Select(cat => cat.Name).Contains(item) || dogs.Select(dog => dog.Name).Contains(item));
            }

        }
        [TestMethod()]
        public void Except()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] result = LinqExtensionMethods.PetConcaterClass.Except(cats, dogs).ToArray();

            foreach (var item in result)
            {
                foreach (var dog in dogs)
                {
                    Assert.IsFalse(dog.Name == item);
                }
            }

        }
        [TestMethod()]
        public void CountPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            int result = LinqExtensionMethods.PetConcaterClass.Count(cats,dogs);

            Assert.AreEqual(result, cats.Length + dogs.Length);
        }
        [TestMethod()]
        public void SumPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            int result = LinqExtensionMethods.PetConcaterClass.Sum(cats, dogs);

            int counter = 0;
            foreach (var item in cats)
            {
                counter += item.Age;
            }
            foreach (var item in dogs)
            {
                counter += item.Age;
            }
            Assert.AreEqual(result, counter);
        }
        [TestMethod()]
        public void MinPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            int result = LinqExtensionMethods.PetConcaterClass.Min(cats, dogs);
            int min = cats[0].Age;
            foreach (var item in cats)
            {
                if(item.Age < min)
                min = item.Age;
            }
            foreach (var item in dogs)
            {
                if (item.Age < min)
                    min = item.Age;
            }
            Assert.AreEqual(result, min);

        }
        [TestMethod()]
        public void MaxPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            int result = LinqExtensionMethods.PetConcaterClass.Max(cats, dogs);
            int max = cats[0].Age;
            foreach (var item in cats)
            {
                if (item.Age > max)
                    max = item.Age;
            }
            foreach (var item in dogs)
            {
                if (item.Age > max)
                    max = item.Age;
            }
            Assert.AreEqual(result, max);

        }
        [TestMethod()]
        public void AvgPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            double result = LinqExtensionMethods.PetConcaterClass.Average(cats, dogs);
            double counter = 0;
            foreach (var item in cats)
            {
                counter += item.Age;
            }
            foreach (var item in dogs)
            {
                counter += item.Age;
            }
            Assert.AreEqual(result, counter/(cats.Length+dogs.Length));

        }
    }
}