﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqAdancedExamples;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class GroupByClassTests
    {

        [TestMethod()]
        public void GroupingByFirstLetterDescendingTest()
        {
            //Change the TestMethod to a Unittest
            {

                string[] foodList = { "carrots", "corn", "curcumber", "cabbage", "broccoli", "beans",
                        "barley", "garlic","ginger", "onions", "olives", "orache", "orka", "radicchio",
                        "parsnip", "pinto beans", "pumpkin" };
                var expected = GroupTest(foodList);

                IDictionary<char, int> result =
                    GroupByClass.GroupingByFirstLetterDescending(foodList);

                foreach (var item in result)
                {
                    Assert.IsTrue(expected[item.Key] == result[item.Key]);
                }

                foreach (var item in result)
                {
                    Console.WriteLine("Buchstabe: {0}, Anzahl: {1}", item.Key, item.Value);
                }


                string[] names = { "Benedikt", "Raphael", "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
                Program.PrintList(names, "Grouping Names by First Letter");
                IDictionary<char, int> groupedFirstLetter =
                    GroupByClass.GroupingByFirstLetterDescending(names);

                foreach (var letter in groupedFirstLetter)
                {
                    Console.WriteLine("{0}:{1}", letter.Key.ToString(), letter.Value);
                }

            }
        }

        public static Dictionary<char, int> GroupTest(string[] words)
        {
            List<string> w = new List<string>(words);
            Dictionary<char, int> d = new Dictionary<char, int>();

            while (w.Count > 0)
            {
                List<string> str = new List<string>();
                foreach (string s in w)
                {
                    if (s[0] == w[0][0])
                    {
                        str.Add(s);
                    }
                }
                d.Add(w[0][0], str.Count());
                foreach (string s in str)
                {
                    w.Remove(s);
                }
            }
            return d;
        }
    }
}