﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System.Linq;
using System.Collections.Generic;

namespace LinqAdvancedTests
{
    [TestClass]
    public class LinqpetMethodsTests
    {
        [TestMethod]
        public void ConctPetsTest()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] query = (string[])LinqExtensionMethods.PetMethods.ConcatArrays(cats, dogs).ToArray();

            Assert.AreEqual(query.Length, cats.Length + dogs.Length);

            foreach (string p in query)
            {
                Assert.IsTrue(cats.Select(c => c.Name).Contains(p) || dogs.Select(d => d.Name).Contains(p));//Überprüft ob in cats oder dogs enthalten
            }
        }

        [TestMethod()]
        public void AveragePetsTest()
        {
            Pet[] pets = Pet.GetPets();
            int sum = 0;

            foreach (Pet p in pets)
                sum += p.Age;

            Assert.AreEqual(LinqExtensionMethods.PetMethods.AvaragePetAge(pets), (double)sum / pets.Length);
        }

        [TestMethod()]
        public void CountPetsTest()
        {
            Pet[] p = Pet.GetPets();
            Assert.AreEqual(p.Length, LinqExtensionMethods.PetMethods.CountPets(p));
        }

        [TestMethod()]
        public void ExceptPetsTest()
        {
            Pet[] pets = Pet.GetPets();
            string[] results = LinqExtensionMethods.PetMethods.Except(pets, new string[] { pets[3].Name });
            
            foreach(string name in results)
            {
                Assert.AreNotEqual(name, pets[3].Name);
            }

            Assert.AreEqual(results.Length, pets.Length - 1);
        }

        [TestMethod()]
        public void IntersectPetsTest()
        {
            Pet[] pets = Pet.GetPets();
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] CatIntersect = LinqExtensionMethods.PetMethods.Intersect(pets, cats);
            string[] DogIntersect = LinqExtensionMethods.PetMethods.Intersect(pets, dogs);

            Assert.AreEqual(CatIntersect.Length, cats.Length);
            Assert.AreEqual(DogIntersect.Length, cats.Length);

            foreach (string c in CatIntersect)
            {
                Assert.IsTrue(cats.Select(p => p.Name).Contains(c));
            }
            foreach (string d in DogIntersect)
            {
                Assert.IsTrue(dogs.Select(p => p.Name).Contains(d));
            }
        }

        [TestMethod()]
        public void MaxPetsTest()
        {
            Pet[] pets = Pet.GetPets();

            int maxage = pets[0].Age;
            for (int i = 1; i < pets.Length; i++)
            {
                if (maxage < pets[i].Age)
                    maxage = pets[i].Age;
            }

            Assert.AreEqual(LinqExtensionMethods.PetMethods.MaxAge(pets), maxage);
        }

        [TestMethod()]
        public void MinPetsTest()
        {
            Pet[] pets = Pet.GetPets();

            int minage = pets[0].Age;
            for (int i = 1; i < pets.Length; i++)
            {
                if (minage > pets[i].Age)
                    minage = pets[i].Age;
            }

            Assert.AreEqual(LinqExtensionMethods.PetMethods.MinAge(pets), minage);
        }

        [TestMethod()]
        public void SumPetsTest()
        {
            Pet[] pets = Pet.GetPets();

            int sum = 0;
            foreach (Pet pet in pets)
                sum += pet.Age;

            Assert.AreEqual(LinqExtensionMethods.PetMethods.SumAge(pets), sum);
        }

        [TestMethod()]
        public void UnionPetsTest()
        {
            Pet[] pets = Pet.GetPets();
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            string[] CatUnion = LinqExtensionMethods.PetMethods.UnionPets(pets, cats);
            string[] DogUnion = LinqExtensionMethods.PetMethods.UnionPets(pets, dogs);

            Assert.AreEqual(CatUnion.Length, pets.Length);
            Assert.AreEqual(DogUnion.Length, pets.Length);
            Assert.AreEqual(DogUnion.Length, CatUnion.Length);

            for (int i = 0; i < pets.Length; i++)
            {
                Assert.IsTrue(pets.Select(p => p.Name).Contains(CatUnion[i]) || cats.Select(c=>c.Name).Contains(CatUnion[i]));
                Assert.IsTrue(pets.Select(p => p.Name).Contains(DogUnion[i]) || dogs.Select(d => d.Name).Contains(DogUnion[i]));
            }
        }
    }
}
