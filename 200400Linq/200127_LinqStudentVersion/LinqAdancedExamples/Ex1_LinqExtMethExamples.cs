﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples
{
    public class NameGenerator
    {
        static string[] names = { "Benedikt", "Raphael",
            "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
        static string[] pupils = { "Nora", "Simon", "Markus", "Christoph",
            "Josef", "Benjamin", "Daniel", "Benedikt", "Peter", "Gregor",
            "Dominik", "Simon", "Peter", "Alexander", "Daniel", "Christina", "Raphael"};
        public static string[] GetNames()
        {
            return names;
        }
        public static string[] GetPupils()
        {
            return pupils;
        }
    }
    public class Pet
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public static Pet[] GetCats()
        {
            Pet[] cats = { new Pet { Name="Barley", Age=8 },
                       new Pet { Name="Boots", Age=4 },
                       new Pet { Name="Whiskers", Age=1 } };
            return cats;
        }
        public static Pet[] GetDogs()
        {
            Pet[] dogs = { new Pet { Name="Bounder", Age=3 },
                        new Pet { Name="Snoopy", Age=14 },
                        new Pet { Name="Fido", Age=9 } };
            return dogs;
        }
    }
    public class LinqExtensionMethods
    {
        #region Easy Extension Methods
        public static string[] SortNames(string[] names)
        {
            IEnumerable<string> result = names.OrderBy(s => s); //order by
            return result.ToArray();
        }

        public static string[] ConcatNames(string[] names, string[] pupils)
        {
            return names.Concat(pupils).ToArray();
        }

        public static string[] RemoveRedundant(string[] names)
        {
            return names.Distinct().ToArray();
        }

        public static string[] UnionNames(string[] names, string[] pupils)
        {
            return pupils.Union(names).ToArray();
        }

        public static string[] ReverseNames(string[] names)
        {
            return names.Reverse().ToArray();
        }
        #endregion

        #region Concat_PetArrays
       
        public class PetConcaterClass
        {
            public static IEnumerable<string> ConcatArrays(Pet[] cats, Pet[] dogs)
            {
                IEnumerable<string> query = cats.Select(cat=> cat.Name).Concat(dogs.Select(dog=>dog.Name)); //Select cats&dogs then Concat
                return query.ToArray();
            }
            public static IEnumerable<string> Union(Pet[] cats, Pet[] dogs)
            {
                return cats.Select(cat => cat.Name).Union(dogs.Select(dog=> dog.Name));
            }
            public static IEnumerable<string> Intersect(Pet[] cats, Pet[] dogs)
            {
                return cats.Select(cat => cat.Name).Intersect(dogs.Select(dog => dog.Name));
            }
            public static IEnumerable<string> Except(Pet[] cats, Pet[] dogs)
            {
                return cats.Select(cat => cat.Name).Except(dogs.Select(dog => dog.Name));
            }
            public static int Count(Pet[] cats, Pet[] dogs)
            {
                return dogs.Count() + cats.Count();
            }
            public static int Sum(Pet[] cats, Pet[] dogs)
            {
                return dogs.Sum(dog => dog.Age) + cats.Sum(cat => cat.Age);
            }
            public static int Min(Pet[] cats, Pet[] dogs)
            {
                return dogs.Select(dog => dog.Age).Concat(cats.Select(cat => cat.Age)).Min();

            }
            public static int Max(Pet[] cats, Pet[] dogs)
            {
                return dogs.Select(dog => dog.Age).Concat(cats.Select(cat => cat.Age)).Max();
            }
            public static double Average(Pet[] cats, Pet[] dogs)
            {
                return (dogs.Select(dog => dog.Age).Concat(cats.Select(cat => cat.Age))).Average();
            }
        }
        //TODO
        //Add Union, Intersect, Except Queries and some Aggregation Operations
        #endregion

    }
}
