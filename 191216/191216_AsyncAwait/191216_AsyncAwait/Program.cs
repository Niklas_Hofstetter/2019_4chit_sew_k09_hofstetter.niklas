﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _191216_AsyncAwait
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop();
        }
        public static async Task<string> AddToBag()
        {
            Random r = new Random();

            if (r.Next(0,3) == 1)
            {
                await Task.Delay(500);
                return "Keyboard has been added to your bag";

            }
            else if (r.Next(0,3) == 2)
            {
                await Task.Delay(500);
                return "Mouse has been added to your bag";
            }
            else
            {
                await Task.Delay(500);
                return "Monitor has been added to your bag";
            }
        }
        public static async Task<string> Purchase(int currentBalance)
        {
            int price = 50;
            currentBalance -= price;
            await Task.Delay(1000);
            return "Thank you for purchasing your current balance is: " + currentBalance;

        }
        public static async void Shop()
        {
            int currentBalance = 200;
            Console.WriteLine("Welcome to myOnlineShop");
            await AddToBag().ConfigureAwait(false);
            await Purchase(currentBalance);
        }
    }
    
}
