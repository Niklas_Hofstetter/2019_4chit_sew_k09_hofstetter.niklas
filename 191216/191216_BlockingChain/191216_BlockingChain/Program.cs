﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _191216_BlockingChain
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }


    public interface IObservable
    {
        void Add(IObserver o);
        void Remove(IObserver o);
        void Notify();
    }
    public interface IObserver
    {
        void Update();
    }

    public class Sensor : IObservable
    {
        private List<IObserver> observers = new List<IObserver>();
        private float speed;

        public void Add(IObserver o)
        {
           observers.Add(o);
        }

        public void Notify()
        {
            foreach(IObserver o in observers)
            {
                o.Update();
            }
        }

        public void Remove(IObserver o)
        {
            observers.Remove(o);
        }

        public float GetSpeed()
        {
            return speed;
        }
        public void SetSpeed(float speed)
        {
            this.speed = speed;
            Notify();
        }
    }
    public class Car : IObserver
    {
        private float speed;
        Sensor detector;
        public Car(Sensor detector)
        {
            this.detector = detector;
        }
        public void Update()
        {
            speed = detector.GetSpeed();
            Display(speed);
        }
        public void Display(float speed)
        {
            Console.WriteLine("Current Speed: " + speed);
        }
    }



}
