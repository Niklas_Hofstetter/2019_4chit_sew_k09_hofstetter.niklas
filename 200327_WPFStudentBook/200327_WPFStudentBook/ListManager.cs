﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200327_WPFStudentBook
{
    class ListManager
    {
        public static List<Student> GetSomeStudents()
        {
            List<Student> l = new List<Student>();
            l.Add(new Student("Eins", "Herr", 18, 1, 0));
            l.Add(new Student("Zwei", "Herr", 19, 2, 1));
            l.Add(new Student("Drei", "Herr", 17, 3, 2));
            l.Add(new Student("Vier", "Herr", 18, 4, 3));
            l.Add(new Student("Fünf", "Herr", 17, 5, 4));
            l.Add(new Student("Sechs", "Herr", 18, 6, 5));
            
            return l;
        }
        public static List<Book> GetSomeBooks()
        {
            List<Book> l = new List<Book>();

            l.Add(new Book(1,"Book1", 100, 22));
            l.Add(new Book(2,"Book2", 89, 12));
            l.Add(new Book(3,"Book3", 255, 25));
            l.Add(new Book(4,"Book4", 180, 17));

            return l;
        }
    }
}
