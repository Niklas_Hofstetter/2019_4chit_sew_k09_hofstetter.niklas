﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200327_WPFStudentBook
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Pages { get; set; }
        public int Price { get; set; }

        public Book() { }
        public Book(int id, string title, int pages, int price)
        {
            Id = id;
            Title = title;
            Pages = pages;
            Price = price;
        }
        
        public override string ToString()
        {
            return "Id: "+Id + " Title: " + Title + " Pages: " + Pages + " Price: " + Price;
        }
    }
}
