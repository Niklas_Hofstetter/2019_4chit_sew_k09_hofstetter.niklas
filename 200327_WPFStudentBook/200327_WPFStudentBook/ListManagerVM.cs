﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Data.SqlClient;

namespace _200327_WPFStudentBook
{
    public class ListManagerVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public static ObservableCollection<Student> Students { get; set; } = new ObservableCollection<Student>();
        public static ObservableCollection<Book> Books { get; set; } = new ObservableCollection<Book>();
        public static ObservableCollection<Order> Orders { get; set; } = new ObservableCollection<Order>();
        static SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\niklas\Documents\database.mdf;Integrated Security=True;Connect Timeout=30");


        public ListManagerVM()
        {
            AddStudentCommand = new AddStudentCommand(this);
            DeleteStudentCommand = new DeleteStudentCommand(this);
            AddBookCommand = new AddBookCommand(this);
            DeleteBookCommand = new DeleteBookCommand(this);
            connection.Open();
            MDFRead();
        }
        ~ListManagerVM()
        {

        }
        public void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #region Students
        public Student currentstudent = new Student();
        private Student selectedStudent = new Student();
        public Student CurrentStudent
        {
            get { return currentstudent; }
            set
            {
                currentstudent = value;
                OnPropertyChanged();
                OnPropertyChanged("SelectedItem");
            }

        }
        public string CurrentLName
        {
            get
            {
                return CurrentStudent.LName;
            }
            set
            {
                CurrentStudent.LName = value;
                OnPropertyChanged();
            }
        }
        public string CurrentFName
        {
            get
            {
                return CurrentStudent.FName;
            }
            set
            {
                CurrentStudent.FName = value;
                OnPropertyChanged();
            }
        }
        public int CurrentAge
        {
            get
            {
                return CurrentStudent.Age;
            }
            set
            {
                CurrentStudent.Age = value;
                OnPropertyChanged();
            }
        }
        public int CurrentSchoolId
        {
            get
            {
                return CurrentStudent.SchoolId;
            }
            set
            {
                CurrentStudent.SchoolId = value;
                OnPropertyChanged();
            }
        }
        public Student SelectedStudent
        {
            get { return selectedStudent; }
            set { selectedStudent = value; }
        }
        public ICommand AddStudentCommand { get; set; }
        public ICommand DeleteStudentCommand { get; set; }
       

        public void AddStudentToList()
        {
            if (Students.Count() == 0)
                CurrentStudent.Id = 1;
            else
                currentstudent.Id = Students.Last().Id + 1;
            Students.Add(currentstudent);
            CurrentStudent = new Student();
            
        }
        public void DeleteStudent()
        {
            Students.Remove(selectedStudent);
        }
        #endregion
        #region Books

        Book currentbook = new Book();
        Book selectedbook = new Book();
        public Book Currentbook
        {
            get { return currentbook; }
            set
            {
                currentbook = value;
                OnPropertyChanged();
                OnPropertyChanged("SelectedItem");
            }
        }
        public string CurrentTitle
        {
            get { return currentbook.Title; }
            set
            {
                currentbook.Title = value;
                OnPropertyChanged();
            }
        }
        public int CurrentPages
        {
            get { return currentbook.Pages; }
            set
            {
                currentbook.Pages = value;
                OnPropertyChanged();
            }
        }
        public int CurrentPrice
        {
            get { return currentbook.Price; }
            set
            {
                currentbook.Price = value;
                OnPropertyChanged();
            }
        }
        public Book SelectedBook
        {
            get { return selectedbook; }
            set { selectedbook = value; }
        }
        public ICommand AddBookCommand { get; set; }
        public ICommand DeleteBookCommand { get; set; }


        public void AddBookToList()
        {
            if (Books.Count() == 0)
                currentbook.Id = 1;
            else
                currentbook.Id = Books.Last().Id+1;
            Books.Add(currentbook);
            currentbook = new Book();
        }
        public void DeleteBook()
        {
            Books.Remove(SelectedBook);
        }
        #endregion
        static void MDFRead()
        {
            SqlCommand select = new SqlCommand("Select * from Students", connection);
            using (SqlDataReader reader = select.ExecuteReader())
            {
                while (reader.Read())
                {
                    Students.Add(new Student() { Id = Convert.ToInt32(reader["Id"]), LName = reader["LName"].ToString(), FName = reader["FName"].ToString(), Age=Convert.ToInt32(reader["Age"]), SchoolId = Convert.ToInt32(reader["SchoolId"]) });
                }
            }
            SqlCommand selectbooks = new SqlCommand("Select * from Books", connection);
            using (SqlDataReader reader = selectbooks.ExecuteReader())
            {
                while (reader.Read())
                {
                    Books.Add(new Book() { Id = Convert.ToInt32(reader["Id"]), Title = reader["Title"].ToString(), Pages = Convert.ToInt32(reader["Pages"]), Price = Convert.ToInt32(reader["Price"]) });
                }
            }
            SqlCommand selectorders = new SqlCommand("Select * from Orders", connection);
            using (SqlDataReader reader = selectorders.ExecuteReader())
            {
                while (reader.Read())
                {
                    Orders.Add(new Order() { OrderId = Convert.ToInt32(reader["OrderId"]), StudentId = Convert.ToInt32(reader["StudentId"]), BookId = Convert.ToInt32(reader["BookId"]) });
                }
            }
        }
            
        public static void MDFSave()
        {
            
            SqlCommand truncatestudent = new SqlCommand("TRUNCATE TABLE Students", connection);
                truncatestudent.ExecuteNonQuery();
                SqlCommand truncatebook = new SqlCommand("TRUNCATE TABLE Books", connection);
                truncatebook.ExecuteNonQuery();
            SqlCommand truncateorders = new SqlCommand("TRUNCATE TABLE Orders", connection);
            truncateorders.ExecuteNonQuery();
            foreach (var stud in Students)
                {
                    SqlCommand insertstudent = new SqlCommand("INSERT INTO Students(Id, LName, FName, SchoolId, Age) VALUES (" + stud.Id + ", '" + stud.LName + "', '" + stud.FName + "', " + stud.SchoolId + ", " + stud.Age + ")", connection);
                    insertstudent.ExecuteNonQuery();
                }
                foreach (var book in Books)
                {
                    SqlCommand insertbook = new SqlCommand("INSERT INTO Books(Id, Title, Pages, Price) VALUES ("+book.Id+", '" + book.Title + "', " + book.Pages + ", " + book.Price + ")", connection);
                    insertbook.ExecuteNonQuery();
                }
            foreach (var order in Orders)
            {
                SqlCommand insertorder = new SqlCommand("INSERT INTO Orders(OrderId, StudentId, BookId) VALUES (" + order.OrderId + ", '" + order.StudentId + "', " + order.BookId + ")", connection);
                insertorder.ExecuteNonQuery();
            }
        }
    }
}
