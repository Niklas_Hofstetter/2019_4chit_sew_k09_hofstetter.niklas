﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200327_WPFStudentBook
{
    public class Abfragen
    {
        public static double Average()
        {
            return ListManagerVM.Students.Average(s => s.Age);
        }
        public static int Sum()
        {
            return ListManagerVM.Students.Sum(s => s.Age);
        }
        public static List<Student> OrderBy()
        {
            return ListManagerVM.Students.OrderBy(s => s.Age).ToList();
        }
        public static List<Student> Descending()
        {
            return ListManagerVM.Students.OrderByDescending(s => s.Age).ToList();
        }
        public static List<Student> Take()
        {
            return ListManagerVM.Students.Take(2).ToList();
        }
        
        

    }
}
