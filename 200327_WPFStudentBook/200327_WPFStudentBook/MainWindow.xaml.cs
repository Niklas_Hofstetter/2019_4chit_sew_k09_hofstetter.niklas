﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _200327_WPFStudentBook
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           throw new NotImplementedException();
        }

        private void btnDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            //ListManagerVM.Students.Remove(ListManagerVM.currentstudent);
        }

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            ListManagerVM.MDFSave();
        }

        private void btnCompile_Click(object sender, RoutedEventArgs e)
        {
            

            if (checkBoxMessage.IsChecked == true)
                MessageBox.Show("Bestätigung");
            if (rbtnAvg.IsChecked == true)
            {
                MessageBox.Show(Abfragen.Average().ToString());
            }
            if (rbtnSum.IsChecked == true)
            {
                MessageBox.Show(Abfragen.Sum().ToString());
            }
            if (rbtnOrderBy.IsChecked == true)
            {
                dgridAbfragen.ItemsSource = Abfragen.OrderBy();
            }
            if (rbtnDesc.IsChecked == true)
            {
                dgridAbfragen.ItemsSource = Abfragen.Descending();
            }
            if (rbtnTake.IsChecked == true)
            {
                dgridAbfragen.ItemsSource = Abfragen.Take();
            }
           

        }

        private void tabControl_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            int i;
            if (ListManagerVM.Orders.Count() == 0)
                i = 1;
            else
                i = ListManagerVM.Orders.Last().OrderId + 1;
            try
            {
                if (sldStudentId.Value == 0 || sldBookId.Value == 0)
                    throw new Exception();
                ListManagerVM.Orders.Add(new Order(i, Convert.ToInt32(sldStudentId.Value), Convert.ToInt32(sldBookId.Value)));
            }
            catch (Exception)
            {
                MessageBox.Show("Diese Bestellung ist nicht möglich!");
            }
        }

        private void sldBookId_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            tblbookId.Text = sldBookId.Value.ToString();
        }

        private void sldStudentId_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            tblStudentId.Text = sldStudentId.Value.ToString();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ListManagerVM.MDFSave();
        }
    }
}
