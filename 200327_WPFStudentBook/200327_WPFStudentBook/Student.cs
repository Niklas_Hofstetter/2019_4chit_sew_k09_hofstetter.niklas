﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200327_WPFStudentBook
{
    public class Student: Person
    {
        public int SchoolId { get; set; }
        
        public string Name
        {
            get
            {
                return this.FName + " " + this.LName;
            }
        }
        public Student() { }
        public Student(string ln, string fn, int age, int sid, int id)
        {
            this.LName = ln;
            this.FName = fn;
            this.Age = age;
            this.SchoolId = sid;
            this.Id = id;
        }

    }
}
