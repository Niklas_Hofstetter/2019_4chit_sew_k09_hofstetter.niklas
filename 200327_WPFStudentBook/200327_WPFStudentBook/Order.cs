﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200327_WPFStudentBook
{
    public class Order
    {
        public int OrderId { get; set; }
        public int StudentId { get; set; }
        public int BookId { get; set; }
        public Order() { }
        public Order(int oid, int sid, int bid)
        {
            OrderId = oid;
            StudentId = sid;
            BookId = bid;
        }
        public override string ToString()
        {
            return "OrderId: " + OrderId + " StudentId: " + StudentId + " BookId: " + BookId;
        }
    }
}
