﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200327_WPFStudentBook
{
    class AddStudentCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        ListManagerVM parent;
        public AddStudentCommand(ListManagerVM p)
        {
            parent = p;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddStudentToList();
        }
    }
    class DeleteStudentCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        ListManagerVM parent;
        public DeleteStudentCommand(ListManagerVM p)
        {
            parent = p;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.DeleteStudent();
        }
    }
    class AddBookCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        ListManagerVM parent;
        public AddBookCommand(ListManagerVM p)
        {
            parent = p;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddBookToList();
        }
    }
    class DeleteBookCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        ListManagerVM parent;
        public DeleteBookCommand(ListManagerVM p)
        {
            parent = p;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.DeleteBook();
        }
    }
}
