﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class Role : AEntity
    {
        public string CounterRole { get; set; }
        public string StrongRole { get; set; }
        public string Description { get; set; }

        //1 Rolle hat mehrere Champions / Mehrere Champs haben 1 Rolle == 1zuN
        public List<Champion> ChampionList { get; set; }
    }
}
