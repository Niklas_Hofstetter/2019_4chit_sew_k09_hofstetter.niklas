﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class Ability
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Dammage { get; set; }

        //1 to 1
        public int ChampionId { get; set; }
        public Champion Champion { get; set; }
    }
}
