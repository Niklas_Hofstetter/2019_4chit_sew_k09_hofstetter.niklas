﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class Item : AEntity
    {
        public int BonusAttackDammage { get; set; }
        public int BonusAbilityDammage { get; set; }
        public int BonusHealthPoints { get; set; }
        public int Price { get; set; }

        //1 Champion hat mehrere Items, ein Item hat mehrere Champions == ChampionItems

    }
}
