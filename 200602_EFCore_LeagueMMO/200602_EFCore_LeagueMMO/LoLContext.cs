﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class LoLContext : DbContext
    {
        public DbSet<Champion> champs {get; set; }
        public DbSet<Item> items {get; set; }
        public DbSet<Role> roles {get; set; }
        public DbSet<Ability> abilities {get; set; }
        public DbSet<Minions> minions {get; set; }
        public DbSet<Skin> skins { get; set; }

        public LoLContext() : base()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=LoLDB;Trusted_Connection=True;");
        }
    }
}
