﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class Minions : AEntity
    {
        public bool isFriendly { get; set; }
        public bool isAgressive { get; set; }
        public bool isAttackingOnContact { get; set; }

        public int Strength { get; set; }
        public int HealthPoints { get; set; }
    }
}
