﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class ChampionItems
    {
        public int ChampionId { get; set; }
        public Champion Champion { get; set; }
        public int ItemsId { get; set; }
        public Item items { get; set; }
    }
}
