﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _200602_EFCore_LeagueMMO.Migrations
{
    public partial class LoLDBContext2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    BonusAttackDammage = table.Column<int>(nullable: false),
                    BonusAbilityDammage = table.Column<int>(nullable: false),
                    BonusHealthPoints = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_items", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "minions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    isFriendly = table.Column<bool>(nullable: false),
                    isAgressive = table.Column<bool>(nullable: false),
                    isAttackingOnContact = table.Column<bool>(nullable: false),
                    Strength = table.Column<int>(nullable: false),
                    HealthPoints = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_minions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    CounterRole = table.Column<string>(nullable: true),
                    StrongRole = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "champs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    CurrMoney = table.Column<int>(nullable: false),
                    HealthPoints = table.Column<int>(nullable: false),
                    AttackDammage = table.Column<int>(nullable: false),
                    AbilityDammage = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_champs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_champs_roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "abilities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Dammage = table.Column<int>(nullable: false),
                    ChampionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abilities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_abilities_champs_ChampionId",
                        column: x => x.ChampionId,
                        principalTable: "champs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "skins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    ChampionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_skins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_skins_champs_ChampionId",
                        column: x => x.ChampionId,
                        principalTable: "champs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abilities_ChampionId",
                table: "abilities",
                column: "ChampionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_champs_RoleId",
                table: "champs",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_skins_ChampionId",
                table: "skins",
                column: "ChampionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "abilities");

            migrationBuilder.DropTable(
                name: "items");

            migrationBuilder.DropTable(
                name: "minions");

            migrationBuilder.DropTable(
                name: "skins");

            migrationBuilder.DropTable(
                name: "champs");

            migrationBuilder.DropTable(
                name: "roles");
        }
    }
}
