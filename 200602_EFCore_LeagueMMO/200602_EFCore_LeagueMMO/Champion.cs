﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200602_EFCore_LeagueMMO
{
    class Champion : AEntity
    {
        public int CurrMoney { get; set; }
        public int HealthPoints { get; set; }
        public int AttackDammage { get; set; }
        public int AbilityDammage { get; set; }
        
        //1-to-1
        public Ability AbilityId { get; set; }

        //1 Champion hat mehrere Skins
        public List<Skin> ChampionSkins { get; set; }

        //1 Champion hat mehrere Items, ein Item hat mehrere Champions == ChampionItems

    }
}
