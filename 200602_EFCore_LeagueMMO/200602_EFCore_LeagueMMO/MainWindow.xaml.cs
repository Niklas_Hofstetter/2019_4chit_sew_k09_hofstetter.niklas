﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _200602_EFCore_LeagueMMO
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            setValues();
            InitializeComponent();
        }
        public void setValues()
        {
            using (var context = new LoLContext())
            {
                var Teemo = new Champion()
                {
                    FullName = "Teemo",
                    ShortName = "The Devil",
                    AbilityDammage = 300,
                    AttackDammage = 150,
                    HealthPoints = 1400,
                    CurrMoney = 500
                };
                var Garen = new Champion()
                {
                    FullName = "Garen",
                    ShortName = "SpinToWin",
                    AbilityDammage = 0,
                    AttackDammage = 190,
                    HealthPoints = 3955,
                    CurrMoney = 500
                };
                var TrinityForce = new Item()
                {
                    FullName = "Trinity Force",
                    ShortName = "Tri Force",
                    BonusAbilityDammage = 0,
                    BonusAttackDammage = 25,
                    BonusHealthPoints = 250
                };
                var BladeRuinedKing = new Item()
                {
                    FullName = "Blade of the Ruined King",
                    ShortName = "BORK",
                    BonusAbilityDammage = 0,
                    BonusAttackDammage = 40,
                    BonusHealthPoints = 0
                };
                var Fighter = new Role()
                {
                    FullName = "Fighter", 
                    ShortName = "bruiser",
                    Description="This dude is tanky and does quite a lot of dammage",
                    StrongRole="Good VS Assasin",
                    CounterRole = "Bad VS Mage"
                };
                var Marksman = new Role()
                {
                    FullName = "Marksman",
                    ShortName = "ADC",
                    Description = "This dude is really squishy but deals in the later stages of the game a lot of dammage",
                    StrongRole = "Good VS Tank",
                    CounterRole = "Bad VS Assasin"
                };
                var Beemo = new Skin()
                {
                    Name = "Beemo", 
                    Price = 10
                };
                var GodGaren = new Skin()
                {
                    Name = "God King Garen",
                    Price = 25
                };
                var Scuttle = new Minions()
                {
                    FullName = "Scuttle Crab",
                    ShortName = "Scooter",
                    HealthPoints = 600,
                    isAttackingOnContact = false,
                    isAgressive = false,
                    isFriendly = true,
                    Strength = 0
                };
                var Cannon = new Minions()
                {
                    FullName = "Cannon Minion",
                    ShortName = "Cannon",
                    HealthPoints = 300,
                    isAttackingOnContact = true,
                    isAgressive = true,
                    isFriendly = false,
                    Strength = 30
                };
                var Baron = new Minions()
                {
                    FullName = "Baron Nashor",
                    ShortName = "Baron",
                    HealthPoints = 11000,
                    isAttackingOnContact = true,
                    isAgressive = false,
                    isFriendly = false,
                    Strength = 95
                };
                var Judgement = new Ability()
                {
                    Name = "Judgement",
                    Dammage = 310
                };
                var NoxiousTrap = new Ability()
                {
                    Name = "Noxious Trap",
                    Dammage = 275
                };

                context.champs.Add(Teemo);
                context.champs.Add(Garen);
                context.items.Add(TrinityForce);
                context.items.Add(BladeRuinedKing);
                context.roles.Add(Fighter);
                context.roles.Add(Marksman);
                context.skins.Add(Beemo);
                context.skins.Add(GodGaren);
                context.minions.Add(Scuttle);
                context.minions.Add(Baron);
                context.minions.Add(Cannon);
                context.SaveChanges();
            }
        }
    }
}
