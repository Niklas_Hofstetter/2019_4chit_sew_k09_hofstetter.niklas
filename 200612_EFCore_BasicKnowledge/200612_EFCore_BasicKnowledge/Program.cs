﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200612_EFCore_BasicKnowledge
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }

    class Student
    {
        public int id { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public int sozialversicherungsnummer { get; set; }
    }

    class StudentDBContext : DbContext
    {
        public DbSet<Student> students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=StudentsDB2;Trusted_Connection=True;");
        }
    }


}
